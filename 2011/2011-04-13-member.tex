\input{../preamble/preamble}
\newcommand \meetingdate {April 13 2011}
\rhead{\footnotesize \meetingdate}

\begin{document}
\selectlanguage{swedish}
\section*{Minutes, Member Meeting  \\ \meetingdate}

\newlength\tempOne%
\settowidth\tempOne{Närvarande:\quad}%
\newlength\tempTwo%
\setlength\tempTwo{\linewidth}%
\addtolength{\tempTwo}{-\tempOne}%

\parbox[t]{\tempOne}{%
  Närvarande:%
}%
\parbox[t]{\tempTwo}{\mbox{}\vspace{-2\baselineskip}\vspace{2pt}%
  \begin{multicols}{3}%
\textit{coming}
    % % Fredrik Ahlsgren,\\
    % Gustav Eek,\\
    % Stefan Kangas,\\
    % Leif-Jöran Olsson,
    % % \\
    % % Dan Ros\'en,\\
    % och \\
    % Stian Rødven Eide %,
    % % \\
    % % Ver Takeo,
    % % och \\
    % % Eva Werner
  \end{multicols}%
}

% Matias 
% Navid
% Rikard
% Takara
% Jens

\begin{enumerate}
\item The meeting was opened.
\item The meeting officials were elected as
  \begin{enumerate}
  \item Stefan as chairperson,
  \item Gustav as secretary, and
  \item Mattias as approver. 
  \end{enumerate}
\item The agenda was adjusted and confirmed.
\item Reports.
  \begin{enumerate}
  \item Economy. Fripost now has 32 members. The party gained \sek\,504 and 7 new members to the association. There has also been donations to the association. Running costs are \sek\,400/month.
  \item Technical report. All members now have an account. There are numerous deficiencies with the system, where the most important is that users can not change passwords. People are referred to earlier minuets and the wiki for more details about the system. One coming issue is in what ways people are to be involved with further developments of the system.
  \item Other reports. Fripost has got some media attention and more will follow. \textit{Gö\-te\-bors\-ke Spio\-nen}, the student union paper, has published an interview with Stefan on Internet, that also will be published in the printed version. Gustav is writing an article that will be published in \textit{Fria Tidningen}. 
  \end{enumerate}

\item Propaganda. A theme is needed for posters and flyers.
  \begin{itemize}
  \item Olof in Malmö has created a poster suggestion that was well appreciated.
  \item The meeting rated illustrations by Jonan Söderberg: The key 8\,p, the mailbox with a man 5\,p, the tiger 4\,p, the stand-alone postbox 2\,p, and the letter 0\,p.
  \item Leif-Jöran can print materials.
    \item Issue ``Future'' bellow contains more propaganda ideas. 
  \item A meeting April 27 will discuss propaganda materials further.
  \end{itemize}

\item Recruiting members. A number of issues were raised about member recruitment.
  \begin{itemize}
  \item Stefan will create a mail template on the wiki for people to send to their address books.
  \item Honesty is important when recruiting members. Do not oversell Fripost, and describe also the shortcomings. On the one hand, one should join Fripost because of the issues the association raise and not for its service. On the other hand, there is no need in being pessimistic. Fripost offer something, that Google and ohters can not: user freedom.
  \item The majority of the members are technically interested males.
  \item Throughout Fripost's history, education in for example the usage off \pgp\ and other encryption, has been a part. More of this could also attract more people.
  \item Anonymity is also something that could attract members.
  \item Navid will create a brainstorming recruiting page on the wiki, starting out with these points.
  \item Further issues will be discussed during the meeting April 27.
  \end{itemize}

\item The future. Again a number of issues were raised.
  \begin{itemize}
  \item Discussions from earlier meetings (e.g. May 10) were presented. See minuets from those meetings.
  \item The current technical solution will hold for more member
    s then needed, with regard to disk space and band with. If more people join, the main server will be moved to a hosting place for better reliability.
  \item Ideas are to order T-skirts and stickers, with or without collars.
  \item The meeting decided to order 1000 stickers, if they cost less than \sek\,900.
  \item Issues that are needed to be addressed further are 
    \begin{itemize}
    \item How is Fripost going to grow bigger?
    \item How to organise work and commitments in a sustainable way?
    \item How to get next generation of administrators? It is important that switches of administrators are made easy, so that the torch can be passed along. This is also crucial for to get people to join and make the service stable.
    \end{itemize}
  \item Fripost's solution should be simple and useable, but also a channel for to get deeper interest and learning more.
  \item We (all members) want to grow dependent on Fripost.
  \item Fripost is also for the breading of the pioneer spirit.
  \item The ideas behind Fripost are sketched in a declaration of principals. The development of that is a good forum for further discussion of Fripost ideals.
  \end{itemize}

\item No other questions were raised. 
\item Next meetings
  \begin{itemize}
  \item Propaganda discussion meeting April 27 at 17.00 followed by ordinary board meeting at 18.00. That on Gnutiken.
  \item Member meeting May 25 at 18.00 at Gnutiken.
  \end{itemize}
\end{enumerate}
    

\parbox{\linewidth}{
  \signatureline{Secretary Gustav Eek}
  \hfill
  \signatureline{Approval Mattias}
}

\end{document}